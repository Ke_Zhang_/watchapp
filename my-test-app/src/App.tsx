import * as React from "react";
import { Component, ClassAttributes } from "react";
import { formattedSeconds } from "./utils";

interface StopwatchProps extends ClassAttributes<Stopwatch> {
    initialSeconds: number;
}

type StopwatchStates = {
    secondsElapsed: number;
    lastClearedIncrementer: ReturnType<typeof setTimeout>;
    laps: Array<number>;
}

class Stopwatch extends Component<StopwatchProps, StopwatchStates> {

    private incrementer: ReturnType<typeof setTimeout>;

    constructor(props: StopwatchProps) {
        super(props);
        this.incrementer = setTimeout(() => {}, 0);
        this.state = {
            secondsElapsed: props.initialSeconds,
            lastClearedIncrementer: setTimeout(() => {}, 0),
            laps: []
        }
    }

    handleStartClick = () => {
        this.incrementer = setInterval(() =>
          this.setState({
            secondsElapsed: this.state.secondsElapsed + 1,
          }), 1000);
    }

    handleStopClick = () => {
        clearInterval(this.incrementer);
        this.setState({
            lastClearedIncrementer: this.incrementer,
        });
    }

    handleResetClick = () => {
        clearInterval(this.incrementer);
        this.setState({
            secondsElapsed: 0,
            laps:[]
        });
    }

    handleLabClick = () => {
      const { laps } = this.state;
        this.setState({
          secondsElapsed: 0,
          laps:laps.concat([this.state.secondsElapsed])
        });
        this.forceUpdate();
    }

    handleDeleteClick = (index: number) => {
        this.setState({
            laps: this.state.laps.filter((currentValue, inde) => inde !== index )
        })
    }

    render() {
        const {
            secondsElapsed,
            lastClearedIncrementer,
            laps
        } = this.state;

        const Lap = (props: { index: number, lap: number, onDelete: () => void }) => (
            <div key={props.index} className="stopwatch-lap">
                <strong>{props.index}</strong>/ {formattedSeconds(props.lap)} <button onClick={props.onDelete} > X </button>
            </div>
        );

        return (
            <div className="stopwatch">
                <h1 className="stopwatch-timer">{formattedSeconds(secondsElapsed)}</h1>

                {(secondsElapsed === 0 || this.incrementer === lastClearedIncrementer
                    ? <button type="button" className="start-btn" onClick={this.handleStartClick}>start</button>
                    : <button type="button" className="stop-btn" onClick={this.handleStopClick}>stop</button>
                )}

                {(secondsElapsed !== 0 && this.incrementer !== lastClearedIncrementer
                    ? <button type="button" onClick={this.handleLabClick}>lap</button>
                    : null
                )}

                {(secondsElapsed !== 0 && this.incrementer === lastClearedIncrementer
                    ? <button type="button" onClick={this.handleResetClick}>reset</button>
                    : null
                )}

                <div className="stopwatch-laps">
                    {laps && laps.map((lap: number, i: number) =>
                        <Lap index={i + 1} lap={lap} onDelete={() => this.handleDeleteClick(i)} />)}
                </div>
            </div>
        );

    }

}

export default Stopwatch;