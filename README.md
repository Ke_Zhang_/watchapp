This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Project Introduction

my-test-app is the fix of the original 'code test' project, I will point out each errors below.
However, I've re-developed it in a different way in the my-test-app-redux project

my-test-app-redux is the Redux version of the same application, which implements local states management to centralize the data. Another cool tool is Apollo-link-state, which has the same goal as Redux..

run them by docker-compose up --build
localhost:3000
localhost:4000

## 1. No IState types defined

As there are several internal states, it is recommend to define each state's type and pass the type defination to the component

## 2. 'this.incrementer' not setup properly

As it is a private variable within the component scope (making it only mutable to the class), and the type is actually 'setTimeout'

## 3. 'laps' should be a state

It is a bad practice to directly create and modify a variable on 'this'.

## 4. use arrow function to auto bind 

bind all methods to 'this' with arrow function

## 5. Set proper return type to 'onDelete' props of Lap

## 6. Set proper parameter types to 'laps.map((lap: number, i: number) =>'

## 7.  Directly modify 'this', such as return () => this.laps.splice(index, 1);

Not recommended to mutate state like this, using this.setState instead would be fine

## 8. Other

There is no proper css style

There is no basic unit test for the component or functions, such as rendering test 'expect(shallowed).toMatchSnapshot();'

helper function moved to utils.ts

## 8. Summary

If this is a commercial product, it could be a MVP for idea prove and prototype purpose, for real world use, we may use a local state management tool to handle local states, also try to not maintain state inside container, means using SFC (React Stateless Functional Component) wherever it is possible to reduce the chance of making massive states everywhere.

