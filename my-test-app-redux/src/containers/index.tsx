import * as React from "react";
import { connect } from "react-redux";
import * as stopWatchActions from "../actions";
import { bindActionCreators, Dispatch } from "redux";
import { RootState } from "../reducers";
import { formattedSeconds } from "./utils";
import '../App.sass';

interface StopwatchProps {
    actions: any;
    initialSeconds: number;
    secondsElapsed: number;
    lastClearedIncrementer: ReturnType<typeof setTimeout>;
    laps: Array<number>;
}

class Stopwatch extends React.Component<StopwatchProps> {

    private incrementer: ReturnType<typeof setTimeout>;

    constructor(props: StopwatchProps) {
        super(props);
        this.incrementer = setTimeout(() => { }, 0);
    }

    handleStartClick = () => {
        this.incrementer = setInterval(() =>
            this.props.actions.SetSecondsElapsed(), 1000);
    }

    handleStopClick = () => {
        clearInterval(this.incrementer);
        this.props.actions.SetLastClearedIncrementer(this.incrementer);
    }

    handleResetClick = () => {
        clearInterval(this.incrementer);
        this.props.actions.SetSecondsElapsed(0)
        this.props.actions.SetLaps(-1)
    }

    handleLabClick = () => {
        const { secondsElapsed } = this.props;
        this.props.actions.SetSecondsElapsed(0)
        this.props.actions.SetLaps(secondsElapsed)
    }

    handleDeleteClick = (index: number) => {
        return () => this.props.laps.splice(index, 1);
    }

    render() {
        const {
            secondsElapsed,
            lastClearedIncrementer,
            laps
        } = this.props;

        const Lap = (props: { index: number, lap: number, onDelete: () => Array<number> }) => (
            <div key={props.index} className="stopwatch-lap">
                <strong>{props.index}</strong>/ {formattedSeconds(props.lap)} <button onClick={props.onDelete} > X </button>
            </div>
        );

        return (
            <div className="stopwatch">
                <h1 className="stopwatch-timer">{formattedSeconds(secondsElapsed)}</h1>
                    <div className="stopwatch-buttons">
                        {(secondsElapsed === 0 || this.incrementer === lastClearedIncrementer
                            ? <button type="button" className="start-btn" onClick={this.handleStartClick}>start</button>
                            : <button type="button" className="stop-btn" onClick={this.handleStopClick}>stop</button>
                        )}

                        {(secondsElapsed !== 0 && this.incrementer !== lastClearedIncrementer
                            ? <button type="button" onClick={this.handleLabClick}>lap</button>
                            : null
                        )}

                        {(secondsElapsed !== 0 && this.incrementer === lastClearedIncrementer
                            ? <button type="button" onClick={this.handleResetClick}>reset</button>
                            : null
                        )}
                    </div>
                <div className="stopwatch-laps">
                    {laps && laps.map((lap: number, i: number) =>
                        <Lap index={i + 1} lap={lap} onDelete={this.handleDeleteClick(i)} />)}
                </div>
            </div>
        );

    }

}

const actions: any = Object.assign({}, stopWatchActions);

function mapStateToProps(state: RootState) {
    return {
        secondsElapsed: state.stopWatchReducer.secondsElapsed,
        lastClearedIncrementer: state.stopWatchReducer.lastClearedIncrementer,
        laps: state.stopWatchReducer.laps
    };
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Stopwatch);