import { createAction } from "redux-actions";
import { SetSecondsElapsedPayload, SetLapsPayload, SetLastClearedIncrementerPayload } from "../interfaces/stopWatchModels";

export const Type = {
    SET_SECONDSELAPSED: "SET_SECONDSELAPSED",
    SET_LAPS: "SET_LAPS",
    SET_LAST_ClEARED_INCREMENTER: "SET_LAST_ClEARED_INCREMENTER"
};

export const SetSecondsElapsed = createAction<SetSecondsElapsedPayload>(
    Type.SET_SECONDSELAPSED
);

export const SetLaps = createAction<SetLapsPayload>(
    Type.SET_LAPS
);

export const SetLastClearedIncrementer = createAction<SetLastClearedIncrementerPayload>(
    Type.SET_LAST_ClEARED_INCREMENTER
);
