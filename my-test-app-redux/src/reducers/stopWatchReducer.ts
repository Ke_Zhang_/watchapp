import { handleActions, Action } from "redux-actions";
import { stopWatchModels, SetSecondsElapsedPayload, SetLapsPayload, SetLastClearedIncrementerPayload } from "../interfaces/stopWatchModels";
import * as watchActions from "../actions";

export interface IState {
    secondsElapsed: number;
    lastClearedIncrementer: ReturnType<typeof setTimeout>;
    laps: Array<number>;
};

const initialState: IState = {
    secondsElapsed: 0,
    lastClearedIncrementer: setTimeout(() => {}, 0),
    laps: []
};

export const stopWatchReducer = handleActions<IState, stopWatchModels>(
    {
        [watchActions.Type.SET_SECONDSELAPSED]: (
            state:IState,
            action: Action<SetSecondsElapsedPayload> 
        ) => {
            return {
                ...state,
                secondsElapsed: action.payload as any === 0 ? 0 : state.secondsElapsed + 1
            }
        },
        [watchActions.Type.SET_LAPS]: (
            state:IState,
            action: Action<SetLapsPayload> 
        ) => {
            return {
                ...state,
                laps: action.payload as any === -1 ? [] : [...state.laps, action.payload as any]
            }
        },
        [watchActions.Type.SET_LAST_ClEARED_INCREMENTER]: (
            state:IState,
            action: Action<SetLastClearedIncrementerPayload> 
        ) => {
            return {
                ...state,
                lastClearedIncrementer: action.payload as any
            }
        },

    },
    initialState
);