import {
    stopWatchReducer,
    IState as StopWatchState
} from "./stopWatchReducer";
import { combineReducers } from "redux";

interface StoreEnhancerState {}

export interface RootState extends StoreEnhancerState {
    stopWatchReducer: StopWatchState;
}

export const rootReducer = combineReducers<RootState>({
    stopWatchReducer: stopWatchReducer as any
});