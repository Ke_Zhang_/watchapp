import { createStore } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootReducer, RootState } from "./index";



export function configureStore(initialState?: RootState) {
    // compose enhancers
    const enhancer = composeWithDevTools();
    // create store
    return createStore(rootReducer, initialState!, enhancer);
}