
export interface stopWatchModels {
    secondsElapsed: number;
    lastClearedIncrementer: ReturnType<typeof setTimeout>;
    lap: number;
}

export interface SetSecondsElapsedPayload {
    secondsElapsed: number;
}

export interface SetLapsPayload {
    lap: number;
}

export interface SetLastClearedIncrementerPayload {
    lastClearedIncrementer: ReturnType<typeof setTimeout>;
}
