import * as React from "react";
import StopWatch from './containers';
import './App.sass';

const App = (): JSX.Element => {
  return (
    <div>
        <StopWatch initialSeconds={0}/>
    </div>
  );
};
export default App;